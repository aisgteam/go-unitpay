package unitpay

import (
    "encoding/json"
    "fmt"
    "net/url"
    "sort"
)

type Params struct {
    kv      map[string]string
    keys    []string
}

func (p *Params) Add(key string, value interface{}) *Params {
    if p.kv == nil {
        p.kv = make(map[string]string)
    }
    var v string
    switch t := value.(type) {
    case int, int32, int64:
        v = fmt.Sprintf("%d", t)
    case float32, float64:
        v = fmt.Sprintf("%g", t)
    case string:
        v = t
    case fmt.Stringer:
        v = t.String()
    }
    if v != "" {
        p.kv[key] = v
        p.keys = append(p.keys, key)
    }
    return p
}

func (p *Params) toSignature() []string {
    sort.Strings(p.keys)
    var res []string
    for _, k := range p.keys {
        res = append(res, p.kv[k])
    }
    return res
}

func (p Params) toQuery() url.Values {
    val := url.Values{}
    for k, v := range p.kv {
        val.Add("params["+k+"]", v)
    }
    return val
}

type ApiMethod interface {
    method() string
    request(*Client) (ResponseObject, error)
}

type InitPayment struct {
    paymentType     PaymentSystem
    sum             Amount
    account         string

}

func (m *InitPayment) method() string {
    return "initPayment"
}

func (m *InitPayment) request(c *Client) (ResponseObject, error) {
    return nil, nil
}

type GetPartner struct {
    login       string
    secretKey   string
}

func (m *GetPartner) method() string {
    return "getPartner"
}

func (m *GetPartner) request(c *Client) (ResponseObject, error) {
    u := c.PrepareUri("/api")
    q := u.Query()
    q.Add("login", m.login)
    q.Add("secretKey", c.secretKey)
    //res := response{}
    return nil, nil
}

type GetPartnerResult struct {
    Balance     float64
    Email       string
}

func (r *GetPartnerResult) Decode(b []byte) error {
    return json.Unmarshal(b, r)
}
