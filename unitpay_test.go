package unitpay

import (
    "fmt"
    "net"
    "testing"
)

func Test_SupportedIps(t *testing.T) {
    ips := []string{
        "31.186.100.49",
        "178.132.203.105",
        "52.29.152.23",
        "52.19.56.234",
        "127.0.0.1", // For debug
    }
    for _, ip := range ips {
        fmt.Printf("%b\n", net.ParseIP(ip))
        //supportedIps = append(supportedIps, net.ParseIP(ip))
    }
}
