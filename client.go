package unitpay

import (
    aiapic "bitbucket.org/aisgteam/ai-api-client"
    aistruct "bitbucket.org/aisgteam/ai-struct"
    "github.com/afex/hystrix-go/hystrix"
)

func NewClient(secretKey string, opt HystrixOptions) (*Client, error) {
    c := &Client{}
    c.Name = "Golang UnitPay Client"
    c.Version = aistruct.Version{1, 0, 0, 0}
    c.UserAgent = c.FullName()
    c.secretKey = secretKey
    err := c.New("https://unitpay.ru")
    if err != nil {
        return nil, err
    }
    c.configure(opt)
    return c, nil
}

type Client struct {
    aiapic.HystrixClient
    secretKey   string
}

func (c *Client) configure(opt HystrixOptions) {
    hystrix.ConfigureCommand(c.Name, opt.toHystrix())
}

func (c *Client) Api(method ApiMethod) (ResponseObject, error) {
    return nil, nil
}

type HystrixOptions struct {
    Timeout                 int
    MaxConcurrentRequests   int
    RequestVolumeThreshold  int
    SleepWindow             int
    ErrorPercentThreshold   int
}

func (o HystrixOptions) toHystrix() hystrix.CommandConfig {
    return hystrix.CommandConfig{
        Timeout:                o.Timeout,
        MaxConcurrentRequests:  o.MaxConcurrentRequests,
        RequestVolumeThreshold: o.RequestVolumeThreshold,
        SleepWindow:            o.SleepWindow,
        ErrorPercentThreshold:  o.ErrorPercentThreshold,
    }
}

func DefaultOptions() HystrixOptions {
    return HystrixOptions{
        Timeout:                50000,
        MaxConcurrentRequests:  300,
        RequestVolumeThreshold: 10,
        SleepWindow:            100,
        ErrorPercentThreshold:  10,
    }
}

//func signature()
