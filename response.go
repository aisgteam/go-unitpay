package unitpay

import (
    "encoding/json"
    "fmt"
)

type ResponseObject interface {
    Decode([]byte) error
}

type ResponseError struct {
    Code ErrorCode `json:"code,omitempty"`
    Msg  string    `json:"message"`
}

func (r ResponseError) Error() string {
    if r.Code != 0 {
        return fmt.Sprintf("%s (%d)", r.Msg, r.Code)
    }
    return r.Msg
}

type ErrorCode int32

const (
    AuthError       = ErrorCode(-32000)
    ParametersError = ErrorCode(-32602)
    InternalError   = ErrorCode(-32603)
)

type response struct {
    Result  json.RawMessage
    Error   *ResponseError
}
