package unitpay

type CashItem struct {
    Name    string      `json:"name"`
    Count   int64       `json:"count"`
    Price   float64     `json:"price"`
    Type    TypeCashier `json:"type"`
}

type TypeCashier string

func (t TypeCashier) String() string {
    return string(t)
}

func (t TypeCashier) MarshalText() ([]byte, error) {
    return []byte(t.String()), nil
}

const (
    Commodity            = TypeCashier("commodity")             // Товар
    Excise               = TypeCashier("excise")                // Подакцизный товар
    Job                  = TypeCashier("job")                   // Работа
    Service              = TypeCashier("service")               // Услуга
    GamblingBet          = TypeCashier("gambling_bet")          // Реализация ставки в азартной игре
    GamblingPrize        = TypeCashier("gambling_prize")        // Выплата выигрыша в азартной игре
    Lottery              = TypeCashier("lottery")               // Реализация лотерейного билета
    LotteryPrize         = TypeCashier("lottery_prize")         // Выплата выигрыша лотерейного билета
    IntellectualActivity = TypeCashier("intellectual_activity") // Интеллектуальная деятельность
    Payment              = TypeCashier("payment")               // Платеж
    AgentCommission      = TypeCashier("agent_commission")      // Агентская комиссия
    Another              = TypeCashier("another")               // Другой
    PropertyRight        = TypeCashier("property_right")        // Имущественные права
    NonOperatingGain     = TypeCashier("non-operating_gain")    // Внецентрализованный доход
    InsurancePremium     = TypeCashier("insurance_premium")     // Страхование
    SalesTax             = TypeCashier("sales_tax")             // Налог с продаж
    ResortFee            = TypeCashier("resort_fee")            // Курортный сбор
)
