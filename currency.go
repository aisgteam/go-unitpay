package unitpay

type Amount struct {
    Value       float64
    Currency    Currency
}

type Currency string

const (
    EUR = Currency("EUR")
    UAH = Currency("UAH")
    BYR = Currency("BYR")
    USD = Currency("USD")
    RUB = Currency("RUB")
)
