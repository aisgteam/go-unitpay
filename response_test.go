package unitpay

import (
    "encoding/json"
    "fmt"
    "testing"
)

func TestResponseDecode(t *testing.T) {

    js := []byte(`{"result": {
    "balance": "14434.33",  
    "email": "partner@gmail.com"
}}`)
    res := response{}
    err := json.Unmarshal(js, &res)
    if err != nil {
        t.Error(err)
    }
    fmt.Printf("%#v\n", res)
}

func TestParamsToSignature(t *testing.T) {
    p := (&Params{}).
        Add("paymentType", "yandex").
        Add("sum", 10.00).
        Add("account", "order413").
        Add("projectId", 1).
        Add("ip", "77.129.27.24").
        Add("resultUrl", "http://вашсайт.ru")
    fmt.Printf("%#v\n", p.toSignature())
}

func TestParamsToQuery(t *testing.T) {
    p := (&Params{}).
        Add("paymentType", "yandex").
        Add("sum", 10.90).
        Add("account", "order413").
        Add("projectId", 1).
        Add("ip", "77.129.27.24").
        Add("resultUrl", "http://вашсайт.ru")
    fmt.Printf("%#v\n", p.toQuery())
}
