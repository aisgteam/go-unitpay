package unitpay

type PaymentSystem string

const (
    MC        = PaymentSystem("mc")
    SMS       = PaymentSystem("sms")
    Card      = PaymentSystem("card")
    WebMoney  = PaymentSystem("webmoney")
    Yandex    = PaymentSystem("yandex")
    Qiwi      = PaymentSystem("qiwi")
    PayPal    = PaymentSystem("paypal")
    LiqPay    = PaymentSystem("liqpay")
    AlfaClick = PaymentSystem("alfaClick")
    Cash      = PaymentSystem("cash")
    ApplePay  = PaymentSystem("applepay")
)

type MobileOperator string

const (
    MTS     = MobileOperator("mts")
    Megafon = MobileOperator("mf")
    Beeline = MobileOperator("beeline")
    Tele2   = MobileOperator("tele2")
    Utel    = MobileOperator("usi")
)

type Country int

const (
    Russia     = Country(7)
    Kazakhstan = Country(77)
    Ukraine    = Country(380)
    Moldova    = Country(373)
    Armenia    = Country(374)
    Estonia    = Country(372)
    Azerbaijan = Country(994)
    SouthKorea = Country(82)
    Panama     = Country(507)
    Latvia     = Country(371)
    Lithuania  = Country(370)
    Kyrgyzstan = Country(996)
    Georgia    = Country(9955)
    Tajikistan = Country(992)
    England    = Country(44)
    Uzbekistan = Country(998)
    Israel     = Country(972)
    Thailand   = Country(66)
    Turkey     = Country(90)
    India      = Country(91)
)
